# Unleash the power of staging files in git

When using git, there are four stages in which your files can be before they
are commited:

1. Untracked
2. Unmodified
3. Modified
4. Staged

![git stages](https://git-scm.com/book/en/v2/images/lifecycle.png)

Untracked files can only be staged directly with `git add <path-to-your-file>`.

Unmodified files have not changed from your commited version.

Modified files have a delta from your staged version.

Staged files are the next ones commited.

The commands you will need are:

- `git status` to check the status of your files
- `git add` to add a file to the stage
- `git reset` to remove the file from the stage. **Be careful, `git reset` can also
  reset your whole working tree**
- `git diff` to see what of the files changed

