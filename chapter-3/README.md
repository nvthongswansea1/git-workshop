# The branch

A branch in git is defined as a series of commits. Remember, that a commit has a parent
commit? This is how a branch in git is defined. To make it more easy and human readable,
you can give that HEAD a name, which is then called a branch.

A branch in git is nothing more than a pointer to a commit. You can see that in your `.git`
directory under `refs/heads/<your-branch-name>`. When you `cat` one of those files, there
is just a commit hash inside.

